#!/usr/bin/env python3
"""Android Builder:

Automatically build multiple Android images.
"""
import argparse
import sys

from pathlib import Path

import anbu

from anbu.config import get_config
from anbu import logging


def main():
    parser = argparse.ArgumentParser(
        prog=sys.argv[0],
        description=sys.modules[__name__].__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('INI', help="INI file for project settings", nargs='?')
    parser.add_argument('--data-dir', help="Directory for Anbu specific data")
    device_parms = parser.add_mutually_exclusive_group()
    device_parms.add_argument('--list-devices',
                              '-l', action="store_true", default=False,
                              help="Show a list of all supported devices.")
    device_parms.add_argument('--show-device',
                              '-d',
                              help="Show detailed device infos.")
    device_parms.add_argument('--update-devices',
                              '-u', action="store_true", default=False,
                              help="Show detailed device infos.")
    device_parms.add_argument('--list-patches',
                              '-p', action="store_true", default=False,
                              help="Show a list of all supported patches.")
    parser.add_argument('--verbose',
                        '-v',
                        help="alias for --log-level=info",
                        action="store_true",
                        default=False)
    parser.add_argument('--log-level',
                        help="choose the log level",
                        choices=['WARN', 'INFO', 'DEBUG'],
                        default='WARN')

    args = parser.parse_args()
    device_args = [args.list_devices, args.show_device, args.update_devices]

    if not args.INI and not any(device_args):
        parser.error("Please specify an INI argument (except "
                     "--list-devices or --show-device or --update-devices is "
                     "given.")
    if args.log_level != 'DEBUG' and args.verbose:
        args.log_level = 'INFO'
    logging.setup_logging(args.log_level)
    log = logging.get_logger("main")

    if any(device_args) and args.data_dir and args.INI:
        parser.error("Please specify the data directory either with the INI "
                     "argument or with --data-dir")

    if any(device_args) and not (args.data_dir or args.INI):
        parser.error("Please specify a data directory either with the INI "
                     "argument or with --data-dir")

    script_dir = Path(__file__).parent
    if args.INI:
        # get project config
        anbu.init_config(args.INI, script_dir)
    else:
        anbu.init_config(args.data_dir, script_dir, just_data_dir=True)

    if args.list_devices:
        anbu.print_supported_devices()
        sys.exit()

    if args.show_device:
        anbu.print_device_info(args.show_device)
        sys.exit()

    if args.update_devices:
        anbu.fetch_devices()
        sys.exit()

    if args.list_patches:
        anbu.list_patches()
        sys.exit()

    anbu.check_deps()

    # setup project wide settings
    logging.setup_file_logging()
    if get_config().sign_builds():
        anbu.setup_signing_keys()

    anbu.build_devices()


if __name__ == "__main__":
    main()
