from anbu.config import get_config, Directory
from anbu.devices import DeviceCode, Git
from anbu.patches import Patch
from anbu.build import save_build_log

import logging
import sys
import shutil

from datetime import date

log = logging.getLogger("anbu.devices.gsi")

PATCH_GIT = {"url": "https://github.com/AndyCGYan/lineage_patches_unified.git", "dir": "gsi_patches", "branch": "lineage-20-light"}
PATCH_DIRS = [
    "patches_platform",
    "patches_treble",
]
EXCLUDE = ["0016-Add-support-for-app-signature-spoofing.patch"]
ARCHES = ['arm64']


class GSI(DeviceCode):
    """Prepare source tree specifically for GSI devices."""

    def _get_patch_dir(self, repo_name):
        """Find correct patch directory.

        The repo_name is something like a_b_c.
        The patch_dir can be something of:
            src_dir / a_b_c /
            src_dir / a / b_c /
            src_dir / a_b / c /
            src_dir / a / b / c /
        """
        def resolve(cur_dir, path_snake):
            if not path_snake:
                return cur_dir
            for x in range(1, len(path_parts := path_snake.split("_"))+1):
                if (cur := cur_dir / "_".join(path_parts[:x])).exists():
                    return resolve(cur, "_".join(path_parts[x:]))
            raise FileNotFoundError

        src_dir = get_config().get_dir(Directory.src)
        try:
            return str(resolve(src_dir, repo_name).relative_to(src_dir))
        except FileNotFoundError:
            raise FileNotFoundError(f"No path found for repo {repo_name}")

    def before_patching(self):
        data_dir = get_config().get_dir(Directory.data)
        root_dir = data_dir / PATCH_GIT["dir"]
        patch_repo = Git(root_dir)
        patch_repo.clone_or_update(PATCH_GIT["url"], PATCH_GIT["branch"])

        for patch_dir in PATCH_DIRS:
            category = root_dir / patch_dir
            patch_files = []
            for repo_root in category.iterdir():
                assert repo_root.is_dir()
                patch_file_dir = self._get_patch_dir(repo_root.name)
                log.debug("Directory for patches: %s", patch_file_dir)
                for patch_file in repo_root.iterdir():
                    if patch_file.name in EXCLUDE:
                        continue
                    patch_files.append({
                        "file": patch_file,
                        "directory": patch_file_dir,
                    })
            patch = Patch({
                "name": patch_dir,
                "description": f"GSI patches for {patch_dir}",
                "versions": {"20.0": {"patch_files": patch_files}}
            })
            log.info("Apply %s", patch.get_name())
            patch.apply(self._dev_conf)

    def before_building(self, shell):
        for arch in ARCHES:
            shell.run(f"lunch lineage_gsi_{arch}_vN-{self._dev_conf.build_type}")
            shell.run("mka systemimage")
        save_build_log()
        raise DeviceCode.EarlyReturn()

    def before_zip_saving(self):
        # save the image, a zip is not created
        zips_dir = get_config().get_dir(Directory.zips)
        zips_dir.mkdir(parents=True, exist_ok=True)
        build_dir = get_config().get_dir(Directory.build)
        build_name = self._dev_conf.get_raw("CUSTOM_BUILD_NAME") or "UNOFFICIAL"
        for arch in ARCHES:
            img_file = build_dir / 'target' / 'product' / f'lineage_gsi_{arch}' / 'system.img'
            if not img_file.is_file():
                log.error("Image not found. Searched for %s", img_file)
                sys.exit(77)
            today = date.today().strftime("%Y%m%d")
            target_name = f'lineage-20.0-{today}-{build_name}-GSI-SIMPLE-{arch}.img'
            shutil.copyfile(img_file, zips_dir / target_name)
            log.warning("Saved %s to %s", target_name, zips_dir)
        log.warning("A zip file does not exist for GSI. OTA is not supported.")
        raise DeviceCode.EarlyReturn()


device_code = GSI()
