import logging
import shutil
import sys
import glob
import subprocess

from pathlib import Path
from contextlib import contextmanager

try:
    from urllib.request import urlopen, Request
except ImportError:
    from urllib2 import urlopen, Request

from .config import Directory, get_config
from .patches import get_patch, append_to_front
from .tools import Shell, run
from .logging import save_build_log
from .devices import (fetch_devices, iterate_dev_confs_with_equal_manifests,
                      group_dev_confs_by_same_patches, DeviceCode)

log = logging.getLogger("anbu.build")

_MUPPETS_GITLAB = "https://gitlab.com/the-muppets/manifest/raw/{}/muppets.xml"
_MUPPETS_GITHUB = (
    "https://raw.githubusercontent.com/TheMuppets/manifests/{}/muppets.xml"
)

_VENDOR_GMS = b"""<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    <project path="vendor/partner_gms" name="lineageos4microg/android_vendor_partner_gms" remote="github" revision="master" />
</manifest>"""


def _download(url):
    request = Request(url, headers={"User-Agent": "Mozilla/5.0"})
    return urlopen(request).read()


def _write_or_leave(path, content):
    if not path.is_file():
        with open(path, "wb") as f:
            f.write(content)


def prepare_sources(device_conf):
    """Prepare the source tree for a specific device."""
    config = get_config()
    src_dir = config.get_dir(Directory.src)
    dev_branch = device_conf.version.as_branch()
    device_conf.code.before_init()
    cmd = [
        "repo",
        "init",
        "--manifest-url=https://github.com/LineageOS/android.git",
        f"--manifest-branch={dev_branch}",
        "--git-lfs",
        # "--worktree",  # better in theory, but does not work
    ]
    src_dir.mkdir(parents=True, exist_ok=True)
    run(cmd, cwd=src_dir)

    local_manifests = src_dir / ".repo" / "local_manifests"
    shutil.rmtree(local_manifests, ignore_errors=True)
    local_manifests.mkdir(parents=True)

    # apply local manifests
    if device_conf.proprietary:
        _write_or_leave(
            local_manifests / "proprietary_gitlab.xml",
            _download(_MUPPETS_GITLAB.format(dev_branch)),
        )
        _write_or_leave(
            local_manifests / "proprietary_github.xml",
            _download(_MUPPETS_GITHUB.format(dev_branch)),
        )

    if device_conf.with_gms:
        _write_or_leave(local_manifests / "gms.xml", _VENDOR_GMS)

    if device_conf.manifest:
        src = device_conf.manifest
        dst = local_manifests / f"zz_{device_conf.codename}.xml"
        log.debug("Copy manifest from %s to %s (and filling in branch)",
                  src, dst)
        with open(src) as inp:
            content = inp.read()
            content = content.replace('{branch}',
                                      device_conf.version.as_branch())
            with open(dst, 'w') as outp:
                outp.write(content)

    if config.clean_before_sync():
        log.info("Resetting")
        try:
            cmd = ["repo", "forall", "--verbose", "--command",
                   "git", "reset", "--quiet", "--hard"]
            run(cmd, cwd=src_dir)
            cmd = ["repo", "forall", "--verbose", "--command",
                   "git", "clean", "--quiet", "-d", "--force"]
            run(cmd, cwd=src_dir)
        except subprocess.CalledProcessError:
            log.warning("git reset/clean ended with an error. This may be "
                        "when repositories are deleted in the manifest "
                        "therefore we are ignoring this.")

    device_conf.code.before_sync()
    log.info("Syncing")
    cmd = [
        "repo", "sync",
        "--verbose",
        "--current-branch",
        "--force-sync",
        "--force-remove-dirty",
        "--no-prune",
        "--detach",
        "--fail-fast"
    ]
    run(cmd, cwd=src_dir)

    build_dir = config.get_dir(Directory.build)
    shutil.rmtree(build_dir, ignore_errors=True)
    build_dir.mkdir(parents=True, exist_ok=False)
    link_build_dir = src_dir / "out"
    link_build_dir.unlink(missing_ok=True)
    link_build_dir.symlink_to(build_dir, target_is_directory=True)


def apply_patches(device_conf):
    """Apply all patches that are specified by the device config.

    Return a set of all modified repositories.
    """
    modified_repos = set()
    src_dir = get_config().get_dir(Directory.src)
    # signing keys
    if get_config().sign_builds():
        log.info("Apply signing config")
        key_dir = get_config().get_dir(Directory.keys)
        link_dir = src_dir / "user-keys"
        link_dir.unlink(missing_ok=True)
        link_dir.symlink_to(key_dir, target_is_directory=True)
        config_mk = src_dir / "vendor" / "lineage" / "config" / "common.mk"
        append_to_front(config_mk,
                        "PRODUCT_DEFAULT_DEV_CERTIFICATE := user-keys/releasekey\n"
                        "PRODUCT_OTA_PUBLIC_KEYS := user-keys/releasekey\n\n")
        modified_repos.add(config_mk.parent)

    # patches
    device_conf.code.before_patching()
    for patch_name in device_conf.patches:
        patch = get_patch(patch_name)
        if patch is None:
            log.error("Patch %s does not exist", patch_name)
            sys.exit(111)
        log.info("Apply %s", patch.get_name())
        patch.apply(device_conf)
        modified_repos.update(patch.get_directories())
    return modified_repos


def build(device_conf):
    """Build the image for a specific device."""
    src_dir = get_config().get_dir(Directory.src)
    env = {}
    env["OUT_DIR"] = "out"
    # env["TARGET_UNOFFICIAL_BUILD_ID"] = "TEST"
    env["TERM"] = "dumb"
    if device_conf.with_gms:
        env["WITH_GMS"] = "true"
        env["GMS_MAKEFILE"] = "gms.mk"
    if device_conf.with_su:
        env["WITH_SU"] = "true"
    if get_config().sign_builds():
        env["SIGN_BUILDS"] = "true"
    if get_config().use_ccache():
        env["USE_CCACHE"] = "1"
        env["ENABLE_CCACHE"] = "1"
        env["CCACHE_EXEC"] = get_config().get_ccache_exec()
        ccache_dir = get_config().get_dir(Directory.ccache)
        c_dir = ccache_dir / "ccache"
        c_dir.mkdir(parents=True, exist_ok=True)
        go_dir = ccache_dir / "gocache"
        go_dir.mkdir(parents=True, exist_ok=True)
        env["CCACHE_DIR"] = str(c_dir)
        env["CCACHE_MAXSIZE"] = get_config().get_ccache_size()
        env["GOCACHE"] = go_dir
    with Shell(src_dir, env=env) as sh:
        sh.run("source build/envsetup.sh")
        device_conf.code.before_building(sh)
        sh.run(f"breakfast {device_conf.codename} {device_conf.build_type}")
        # # sh.run(f"mka -j{multiprocessing.cpu_count()} bacon")
        sh.run("mka bacon")
    save_build_log()


def save_zips(device_conf):
    """Save the system images to zips_dir."""
    device_conf.code.before_zip_saving()
    zips_dir = get_config().get_dir(Directory.zips)
    zips_dir.mkdir(parents=True, exist_ok=True)
    build_dir = get_config().get_dir(Directory.build)
    target_dir = build_dir / 'target' / 'product' / device_conf.codename
    build_name = device_conf.get_raw("CUSTOM_BUILD_NAME") or "UNOFFICIAL"
    pattern = (f"{target_dir}/{device_conf.version.as_branch()}-*-"
               f"{build_name}-{device_conf.codename}.zip")
    zip_file = glob.glob(pattern)
    if len(zip_file) == 0:
        log.error("Image not found. Searched for %s", pattern)
        sys.exit(77)
    if len(zip_file) > 1:
        log.error("Multiple images found. Searched for %s", pattern)
        sys.exit(78)
    zip_file = Path(zip_file[0])
    sha_file = zip_file.parent / f"{zip_file.name}.sha256sum"
    assert sha_file.exists(), f"SHA file is missing {sha_file}"
    shutil.copyfile(zip_file, zips_dir / zip_file.name)
    shutil.copyfile(sha_file, zips_dir / sha_file.name)
    log.warning("Saved %s and %s to %s",
                zip_file.name,
                sha_file.name,
                zips_dir)


def clean_src_dir(patch_dirs):
    """Cleanup the src tree."""
    # TODO


@contextmanager
def early_return():
    """Support early return for DeviceCode."""
    try:
        yield
    except DeviceCode.EarlyReturn:
        pass


def build_devices():
    """Trigger the whole build process for all wanted devices."""
    # work on up-to-date info
    fetch_devices()
    for dev_confs in iterate_dev_confs_with_equal_manifests():
        # prepare src for the first device is enough since all of them can
        # operate on the same source tree
        log.info("Prepare device with config: %s", dev_confs[0])
        with early_return():
            prepare_sources(dev_confs[0])
        for s_devices in group_dev_confs_by_same_patches(dev_confs):
            log.info("Apply patches to: %s", s_devices[0])
            patch_dirs = apply_patches(s_devices[0])
            for device_conf in s_devices:
                log.info("Build devices with config: %s", device_conf)
                with early_return():
                    build(device_conf)
                with early_return():
                    save_zips(device_conf)
            clean_src_dir(patch_dirs)
