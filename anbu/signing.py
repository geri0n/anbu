import subprocess
import logging

from .config import get_config, Directory

log = logging.getLogger("anbu.keys")


def _generate_key(key_prefix, subject):
    """Generate a new key pair.

    Oriented on https://android.googlesource.com/platform/development/+/master/tools/make_key
    """
    log.info("Generating key %s with subject %s", key_prefix, subject)
    priv_key = subprocess.run(["openssl", "ecparam", "-name", "prime256v1",
                               "-genkey", "-noout"],
                              capture_output=True, check=True)
    subprocess.run(["openssl", "req", "-new", "-x509", "-sha256",
                    "-key", "-",
                    "-out", f"{key_prefix}.x509.pem", "-days", "10000",
                    "-subj", subject], input=priv_key.stdout, check=True)
    subprocess.run(["openssl", "pkcs8", "-topk8", "-outform", "DER",
                    "-out", f"{key_prefix}.pk8", "-nocrypt"],
                   input=priv_key.stdout, check=True)


def setup_signing_keys():
    """Initialize the Android signing keys, if not already present."""
    keys_dir = get_config().get_dir(Directory.keys)
    keys_dir.mkdir(mode=0o700, parents=True, exist_ok=True)

    subject = get_config().get_key_subject()

    for key in ["releasekey", "platform", "shared", "media", "networkstack",
                "testkey", "sdk_sandbox", "bluetooth"]:
        log.debug("Generate %s key, if not present.", key)
        key_name = key + ".pk8"
        if (keys_dir / key_name).exists():
            continue
        _generate_key(keys_dir / key, subject)
