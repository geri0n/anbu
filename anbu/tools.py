"""Everything regarding using external tools."""

import array
import collections
import logging
import os
import sys
import subprocess
import pty
import io
import select
import errno

from shutil import which

import pexpect

log = logging.getLogger("anbu.tools")

deps = [
    "bash",
    "bc",
    "bison",
    "ccache",
    "clang",
    "git",
    "grep",
    "wget",
    "yasm",
    "repo",
    "openssl",
    "patch",
    "sha256sum",
]


def _check_dep(dep):
    log.debug("Check for dependency %s.", dep)
    prog = which(dep)
    if prog is None:
        log.error("Cannot find the tool '%s'. "
                  "Please ensure that it is in PATH.",
                  dep)
        sys.exit(89)


def check_deps():
    """Check for project wide dependencies."""
    global deps
    for dep in deps:
        _check_dep(dep)


class Shell:
    class _LogWrapper:
        def __init__(self, prompt):
            self._prompt = prompt

        def write(self, data):
            out = data.replace(self._prompt, "")
            for line in out.splitlines():
                log.debug(line)

        def flush(self):
            sys.stdout.flush()

    def __init__(self, cwd, env=None):
        self._prompt = "xie6Aec6>>"
        if env is None:
            env = {}
        env["PATH"] = os.environ.get("PATH", "")
        env["PS1"] = self._prompt
        self._sh = pexpect.spawn("/bin/bash", ["--noprofile",
                                               "--noediting",
                                               "--norc",
                                               "-i"],
                                 cwd=cwd, env=env, encoding="UTF-8")
        self._sh.setecho(False)
        self._sh.expect(self._prompt)
        self._log_wrapper = Shell._LogWrapper(self._prompt)
        self._sh.logfile_read = self._log_wrapper
        # mask the prompt so _LogWrapper.write is not triggered
        masked_prompt = f"'{self._prompt[0]}''{self._prompt[1:]}'"
        self.run(f"env | grep -v {masked_prompt} | sort")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self._sh.terminate(force=True)

    def run(self, cmd):
        log.info("Running %s", cmd)
        self._sh.sendline(cmd)
        self._sh.expect(self._prompt, timeout=None)

        # return value handling
        self._sh.logfile_read = None
        self._sh.sendline("echo $?")
        self._sh.expect(self._prompt, timeout=None)
        self._sh.logfile_read = self._log_wrapper
        retval = int(self._sh.before.strip())
        if retval > 0:
            raise subprocess.CalledProcessError(returncode=retval, cmd=cmd)


class StringBuf:
    """Fill in bytes. Get str lines out of it."""
    @staticmethod
    def _get_byte_buf():
        return array.array('B')

    def __init__(self):
        self._byte_buf = StringBuf._get_byte_buf()
        self._lines = collections.deque()
        self._newlines = [b'\r\n', b'\n', b'\x1b[K']
        self._ends = {x[-1] for x in self._newlines}

    def _bytes_to_line(self, check=False):
        """The current array may be a line. Check and convert."""
        line = self._byte_buf.tobytes()
        if check:
            for nl in self._newlines:
                if line.endswith(nl):
                    line = line[:-len(nl)]
                    break
            else:
                # We did not find a newline but should check for one.
                return

        # there are cases where there remains a carriage return
        # strip that away, too
        line = line.strip(b'\r').decode('UTF-8')
        if line:
            self._lines.append(line)
        self._byte_buf = StringBuf._get_byte_buf()

    def append_bytes(self, byte_s):
        """Append bytes to the buffer."""
        if not byte_s:
            # EOF, write remaining bytes
            self._bytes_to_line()
            return

        for byte in byte_s:
            # print(byte, repr(byte))
            self._byte_buf.append(byte)
            if byte in self._ends:
                # this may be a newline
                self._bytes_to_line(check=True)

    def has_lines(self):
        """Check if there are new lines."""
        return bool(self._lines)

    def get_line(self):
        """Return the first line (consuming)."""
        return self._lines.popleft()


def run(cmd, cwd=None, input=None):
    log.info("Run %s in %s", " ".join(cmd), cwd)
    main_cmd = cmd[0]
    assert main_cmd in deps, f"Command {main_cmd} is not in deps"

    # some processes (especially repo) want a TTY to give a nice output
    # fake one as a consequence
    server_out, client_out = pty.openpty()
    server_err, client_err = pty.openpty()

    popen_kwargs = {'cwd': cwd,
                    'stdout': client_out,
                    'stderr': client_err,
                    'env': {**os.environ, **{'TERM': 'dumb'}}}
    if input is not None:
        popen_kwargs['stdin'] = subprocess.PIPE

    s_log = log.getChild(main_cmd)

    with subprocess.Popen(cmd, **popen_kwargs) as process:
        # we can close the client fds on our side
        os.close(client_out)
        os.close(client_err)

        readable = [server_out, server_err]
        stdout = StringBuf()
        stderr = StringBuf()
        f_map = {server_out: (stdout, s_log.debug),
                 server_err: (stderr, s_log.info)}

        try:
            if input is not None:
                stdin = io.TextIOWrapper(process.stdin, write_through=True,
                                         line_buffering=False)
                stdin.write(input)
                stdin.close()

            while readable:
                ready, _, _ = select.select(readable, [], [], 0.1)
                for fd in ready:
                    buf, log_c = f_map[fd]
                    try:
                        data = os.read(fd, 512)
                    except OSError as e:
                        if e.errno != errno.EIO:
                            raise
                        # EIO means EOF on some systems
                        data = None
                        readable.remove(fd)
                    else:
                        if not data:  # EOF
                            readable.remove(fd)
                    buf.append_bytes(data)
                    while buf.has_lines():
                        log_c(buf.get_line())
        except:  # Including KeyboardInterrupt, communicate handled that.
            process.kill()
            # We don't call process.wait() as .__exit__ does that for us.
            raise

        finally:
            os.close(server_out)
            os.close(server_err)

        retcode = process.poll()
        if retcode:
            raise subprocess.CalledProcessError(retcode, process.args,
                                                output=stdout, stderr=stderr)
