import logging
import shutil

from datetime import datetime

from .config import Directory, get_config

prefix = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")


def setup_logging(log_level: str):
    root = logging.getLogger(name=None)
    root.setLevel(logging.DEBUG)

    hdlr = logging.StreamHandler()
    hdlr.setLevel(log_level)
    hdlr.setFormatter(
        logging.Formatter('%(levelname)-5s %(name)-5s  %(message)s')
    )
    root.addHandler(hdlr)


def get_logger(name: str):
    return logging.getLogger(name)


def setup_file_logging():
    root = logging.getLogger(name=None)
    logs_dir = get_config().get_dir(Directory.logs)
    logs_dir.mkdir(parents=True, exist_ok=True)

    log_file = logs_dir / f"{prefix}_anbu.log"
    hdlr = logging.FileHandler(log_file)
    hdlr.setLevel(logging.DEBUG)
    hdlr.setFormatter(
        logging.Formatter('%(asctime)s - %(levelname)s - %(name)s: %(message)s')
    )
    root.addHandler(hdlr)


def save_build_log():
    logs_dir = get_config().get_dir(Directory.logs)
    build_dir = get_config().get_dir(Directory.build)
    shutil.copyfile(build_dir / 'verbose.log.gz',
                    logs_dir / f"{prefix}_verbose.log.gz")
