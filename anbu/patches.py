import hashlib
import json
import logging
import shutil
import subprocess
import sys
import textwrap

from dataclasses import dataclass
from pathlib import Path

from .config import get_config, Directory
from .tools import run


log = logging.getLogger("anbu.patches")


def mark_repo(repo, key):
    """Make a commit in the repo that marks it with key."""
    # extra commit to mark the patch as applied
    cmd = ["git", "commit", "--allow-empty", f"--message={key}"]
    run(cmd, cwd=repo)


def is_repo_marked(repo, key):
    """Check if repo is already marked with key by search the history."""
    # the extra grep forces the correct cmd return value
    cmd = (f"git log --grep '{key}' --format=oneline --max-count=1 HEAD "
           "--not $(git branch -a --no-contains=HEAD --format='%(objectname)')"
           "| grep .")
    return subprocess.run(cmd, shell=True, cwd=repo, check=False,
                          stdout=subprocess.DEVNULL,
                          stderr=subprocess.DEVNULL).returncode == 0


def append_to_front(path, line):
    sha = hashlib.sha256(line.encode('UTF-8')).hexdigest()
    human_readable = line[:10].replace('\n', '')
    key = f"atf {human_readable} {sha}"
    if is_repo_marked(path.parent, key):
        log.debug("Append to front to %s already happened: %s",
                  path, human_readable)
        return
    with open(path, "r+") as f:
        content = f.read()
        content = line + "\n" + content
        f.seek(0)
        f.write(content)
        mark_repo(path.parent, key)


def setup_overlay():
    src_dir = get_config().get_dir(Directory.src)
    overlay_dir = src_dir / "vendor" / "lineage" / "overlay" / "microg"
    overlay_dir.mkdir(parents=False, exist_ok=True)

    append_to_front(src_dir / "vendor" / "lineage" / "config" / "common.mk",
                    "PRODUCT_PACKAGE_OVERLAYS := vendor/lineage/overlay/microg")

    return overlay_dir


@dataclass
class Parameter:
    name: str
    description: str


class Patch:
    def __init__(self, description, working_dir=None):
        self._desc = description
        self._wd = working_dir

        # sanity checks
        assert " " not in self.get_name()

    def __repr__(self):
        return f"Patch({self.get_name()})"

    def get_name(self):
        return self._desc["name"]

    def get_description(self):
        return self._desc["description"]

    def get_parameters(self):
        parms = {}
        for parm in self._desc.get("parameter", []):
            parms[parm["name"]] = Parameter(name=parm["name"],
                                            description=parm["description"])
        return parms

    def get_directories(self, device_conf):
        """Get a set of all directories that the patch modifies."""
        src_dir = get_config().get_dir(Directory.src)
        b_desc = self._get_versioned_description(device_conf)
        for patch in b_desc.get("patch_files", []):
            patch_dir = src_dir / patch["directory"]
            assert patch_dir.is_dir(), f"{patch_dir} is not a directory."

    def _apply_parameter(self, device_conf, file_info, content):
        for parm in file_info.get("parameter", []):
            value = device_conf.get_raw(parm)
            if value is None:
                log.error("The patch %s needs an argument %s.",
                          self.get_name(), parm)
                sys.exit(113)
            content = content.replace(f"{{{parm}}}", value)
        return content

    def _get_full_path(self, path):
        if self._wd:
            assert isinstance(self._wd, Path)
            p = self._wd / path
        else:
            assert isinstance(path, Path)
            p = path
        return p.resolve().absolute()

    def _get_versioned_description(self, device_conf):
        """Return the part of the patch description that is needed for the current device."""
        versions = self._desc["versions"]
        cur_version = device_conf.version.as_version()
        if cur_version not in versions:
            log.error("Invalid version %s for patch %s",
                      cur_version, self.get_name())
            sys.exit(112)
        b_desc = versions[cur_version]
        if "all" in versions:
            b_desc = {**versions["all"], **b_desc}

    def apply(self, device_conf):
        # get branch config
        src_dir = get_config().get_dir(Directory.src)
        b_desc = self._get_versioned_description(device_conf)

        # overlay handling
        if "overlay_files" in b_desc:
            overlay_path = setup_overlay()
            for file in b_desc["overlay_files"]:
                from_f = self._get_full_path(file['from'])
                with open(from_f) as f:
                    from_content = f.read()
                from_content = self._apply_parameter(device_conf, file,
                                                     from_content)
                to_f = overlay_path / file["to"]
                to_f.parent.mkdir(parents=True, exist_ok=True)
                with open(to_f, "w") as f:
                    f.write(from_content)

        # actual patches
        for patch in b_desc.get("patch_files", []):
            patch_file = self._get_full_path(patch["file"])
            assert patch_file.is_file()
            patch_dir = src_dir / patch["directory"]
            assert patch_dir.is_dir(), f"{patch_dir} is not a directory."

            unique_name = f"patch {self.get_name()} {patch_file}"
            if is_repo_marked(patch_dir, unique_name):
                log.debug("Patch %s already applied", unique_name)
                continue

            log.debug("Use patch file: %s", patch_file)
            with open(patch_file) as f:
                patch_content = f.read()
            patch_content = self._apply_parameter(device_conf, patch,
                                                  patch_content)
            # try git first and plain patch afterwards
            try:
                cmd = ["git", "am", "-"]
                run(cmd, cwd=patch_dir, input=patch_content)
            except subprocess.CalledProcessError as e:
                log.debug("git am failed with %d", e.returncode)
                cmd = ["git", "am", "--abort"]
                run(cmd, cwd=patch_dir)
                try:
                    cmd = ["git", "apply", "-"]
                    run(cmd, cwd=patch_dir, input=patch_content)
                except subprocess.CalledProcessError as e2:
                    log.debug("git apply failed with %d", e2.returncode)
                    cmd = ["patch", "--quiet",  "--force",
                           "--no-backup-if-mismatch",
                           "--version-control", "none",
                           "-p1"]
                    run(cmd, cwd=patch_dir, input=patch_content)
                finally:
                    # make a commit to make repo happy
                    cmd = ["git", "add", "-A"]
                    run(cmd, cwd=patch_dir)
                    cmd = ["git", "commit",
                           f"--message={self.get_name()}",
                           f"--message={self.get_description()}"]
                    run(cmd, cwd=patch_dir)

            # extra commit to mark the patch as applied
            mark_repo(patch_dir, unique_name)


_patches = None


def _get_patches():
    global _patches
    # early return
    if _patches is not None:
        return _patches

    _patches = {}
    patch_dir = get_config().get_dir(Directory.anbu) / 'patches'
    files = patch_dir.glob('**/description.json')
    for desc_file in files:
        with open(desc_file) as f:
            desc = json.load(f)
        patch = Patch(desc, desc_file.parent)
        _patches[patch.get_name()] = patch

    return _patches


def list_patches():
    len_name = max([len(x) for x in _get_patches().keys()])
    cols = shutil.get_terminal_size((80, 20)).columns
    len_desc = cols - len_name - 2

    head = f"{{0:{len_name}s}} {{1}}"
    print(head.format("Patch", "Description"))
    print(len_name * '-' + ' ' + (len_desc) * '-')

    for patch in _get_patches().values():
        indent = ' ' * (len_name + 1)
        print(patch.get_name(),
              '\n'.join(textwrap.wrap(patch.get_description(),
                                      width=len_desc,
                                      subsequent_indent=indent)))


def get_patch(patch_name):
    return _get_patches().get(patch_name, None)
