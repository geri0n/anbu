import importlib
import importlib.util
import json
import logging
import subprocess
import sys
import yaml

from functools import lru_cache, total_ordering
from dataclasses import dataclass
from typing import Tuple
from pathlib import Path

from .config import get_config, Directory, Version, Status
from .patches import Patch

LINEAGE_GIT = {"url": "https://github.com/LineageOS/lineage_wiki.git", "dir": "lineage_wiki", "branch": "master"}

log = logging.getLogger("anbu.devices")


class DeviceCode:
    """Device specific code that anbu executes like a plugin"""
    _err_msg = "No '%s' needed for this device."

    class EarlyReturn(Exception):
        """Not an error but this specific device wants an early return.

        Every function can call this exception which results aborting the
        calling function.
        """

    def set_config(self, device_config):
        self._dev_conf = device_config
        self._log = log.getChild(self._dev_conf.codename)

    def before_init(self):
        self._log.debug(DeviceCode._err_msg, "before_init")

    def before_sync(self):
        self._log.debug(DeviceCode._err_msg, "before_sync")

    def before_patching(self):
        self._log.debug(DeviceCode._err_msg, "before_patching")

    def before_building(self, shell):
        self._log.debug(DeviceCode._err_msg, "before_building")


@dataclass
class DeviceQuirks:
    """Device specific quirks that must hold for a successful build."""
    manifest: Path = None
    manifest_overrides: bool = None
    patches: Tuple[Patch] = None
    proprietary: bool = None
    code: DeviceCode = None

    def __bool__(self):
        return (self.manifest is not None or
                self.patches is not None or
                self.proprietary is not None)

    def affect(self, config_key):
        if config_key == 'manifest' and self.manifest is not None:
            return True
        if config_key == 'proprietary' and self.proprietary is not None:
            return True
        return False

    @staticmethod
    def from_desciption(desc, device_dir):
        dq = DeviceQuirks()
        if 'manifest_file' in desc:
            dq.manifest = (device_dir / desc['manifest_file']).resolve().absolute()
            dq.manifest_overrides = desc['manifest_overrides']
        if 'patches' in desc:
            raise NotImplementedError()
        if 'include_proprietary' in desc:
            dq.proprietary = desc['include_proprietary']
        if "exec" in desc:
            script_path = (device_dir / desc['exec']).resolve().absolute()
            log.debug("Device needs extra code. Load %s", script_path)
            loader = importlib.machinery.SourceFileLoader(f'anbu.devices.gsi', str(script_path))
            spec = importlib.util.spec_from_loader(loader.name, loader)
            mod = importlib.util.module_from_spec(spec)
            loader.exec_module(mod)
            dq.code = mod.device_code
        return dq


@total_ordering
@dataclass
class SupportedVersion:
    version: Version
    status: Status
    quirks: DeviceQuirks = None

    def __str__(self):
        sts = f' ({self.status.short()})' if self.status != Status.official else ''
        return f"{self.version}{sts}"

    def __eq__(self, other):
        return self.version == other.version

    def __lt__(self, other):
        return self.version < other.version


@dataclass
class Device:
    """All information that belongs to a specific device."""
    codename: str
    name: str
    supported_versions: Tuple[SupportedVersion]
    # config: Optional[DeviceConfig] = None  # DeviceConfig | None for 3.10

    @staticmethod
    def from_yaml(device):
        return Device(codename=device['codename'],
                      name=f'{device["vendor"]} {device["name"]}',
                      supported_versions=tuple([SupportedVersion(version=Version(str(x)),
                                                                 status=Status.official)
                                                for x in device['versions']]))

    @staticmethod
    def from_description(description, device_dir):
        all_quirks = description['versions'].get('all', {})
        versions = []
        for name, quirks in description['versions'].items():
            if name == 'all':
                continue
            g_quirks = {**all_quirks, **quirks}
            versions.append(SupportedVersion(
                version=Version(name),
                status=Status[g_quirks['status']],
                quirks=DeviceQuirks.from_desciption(g_quirks, device_dir)))

        return Device(codename=description['codename'],
                      name=description['name'],
                      supported_versions=tuple(versions))


class Git:
    def __init__(self, path):
        self._path = path
        self._shallow_args = ["--depth", 20]
        self._log = logging.getLogger("anbu.git")

    def _exec_git(self, args, in_git=True):
        cd = []
        if in_git:
            cd = ['-C', str(self._path)]
        cmd = ["git", *cd, *map(str, args)]
        self._log.debug("Execute: %s", ' '.join([f'{x}' for x in cmd]))
        subprocess.run(cmd)

    def is_repo(self):
        return (self._path / ".git" / "HEAD").exists()

    def update(self, branch):
        self._exec_git(["fetch", *self._shallow_args])
        self._exec_git(["checkout", branch])
        self._exec_git(["reset", "--hard", f"origin/{branch}"])

    def clone(self, url, branch):
        self._exec_git(["clone", "--branch", branch, *self._shallow_args, url, self._path], in_git=False)

    def clone_or_update(self, url, branch):
        if self.is_repo():
            self.update(branch)
        else:
            self.clone(url, branch)


def _get_wiki_dir():
    data_dir = get_config().get_dir(Directory.data)
    return data_dir / LINEAGE_GIT["dir"]


def _get_wiki_or_error():
    wiki_dir = _get_wiki_dir()
    if not Git(wiki_dir).is_repo():
        log.error("Device Repository not existent. "
                  "Please execute '--update-devices' first.")
        sys.exit(23)
    return wiki_dir


def fetch_devices():
    wiki_dir = _get_wiki_dir()
    wiki = Git(wiki_dir)
    wiki.clone_or_update(LINEAGE_GIT["url"], LINEAGE_GIT["branch"])


@lru_cache
def get_official_devices():
    # official devices
    wiki_dir = _get_wiki_or_error()
    devices = {}
    for device_file in (wiki_dir / "_data" / "devices").glob("*"):
        with open(device_file) as f:
            dev_yaml = yaml.safe_load(f)
            devices[dev_yaml["codename"]] = Device.from_yaml(dev_yaml)
    return devices


@lru_cache
def get_supported_devices():
    devices = get_official_devices()

    # unofficial devices
    device_dir = get_config().get_dir(Directory.anbu) / 'devices'
    files = device_dir.glob('**/description.json')
    for desc_file in files:
        with open(desc_file) as f:
            desc = json.load(f)
        device = Device.from_description(desc, desc_file.parent)
        if device.codename in devices:
            devices[device.codename] = devices[device].extend(device)
        else:
            devices[device.codename] = device

    return devices


def print_supported_devices():
    devices = get_supported_devices()

    def data_lens(key):
        return [len(str(getattr(x, key))) for x in devices.values()]

    len_codename = max(data_lens("codename"))
    lens_name = sorted(data_lens("name"))
    len_name = lens_name[int(len(lens_name)*0.98)+1]

    head = f"{{0:{len_codename}s}} {{1:{len_name}}} {{2}}"
    head = head.format("Codename", "Name", "Supported Versions (U = Unofficial, C = Community)")
    print(head)
    print(len(head) * '-')
    for dev in sorted(devices.values(), key=lambda x: getattr(x, 'codename')):
        form = f"{{0:{len_codename}s}} {{1:{len_name}}} {{2}}"
        print(form.format(dev.codename, dev.name,
                          ', '.join([str(x) for x in dev.supported_versions])))


def print_device_info(device_name):
    device = get_supported_devices()[device_name]
    print(device)


def _get_device_configs():
    cur_devices = []
    for cn in get_config().get_codenames():
        if cn == "all-official":
            all = get_official_devices().keys()
        else:
            all = [cn]
        for codename in all:
            device = get_supported_devices().get(codename, None)
            if device is None:
                log.error("Device '%s' is not supported. Skipping...",
                          device)
                continue
            dev_conf = get_config().get(device)
            cur_devices.append(dev_conf)
    return cur_devices


def iterate_dev_confs_with_equal_manifests():
    dev_confs = _get_device_configs()
    # TODO dummy
    return [[x] for x in dev_confs]


def group_dev_confs_by_same_patches(dev_confs):
    # TODO dummy
    return [[x] for x in dev_confs]
