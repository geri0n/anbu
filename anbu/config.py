from configparser import ConfigParser, ExtendedInterpolation

import logging

from enum import Enum
from dataclasses import dataclass
from functools import total_ordering
from pathlib import Path
from typing import get_type_hints, List


_config = None
log = logging.getLogger("anbu.config")


class Status(Enum):
    official = 1  # officially maintained within LineageOS
    unofficial = 2  # LineageOS repos/branches exist, but is unmaintained
    community = 3  # maintained by the community

    def short(self):
        return self.name[0].upper()


class Directory(Enum):
    src = 1
    data = 2
    build = 3
    keys = 4
    anbu = 5
    ccache = 6
    logs = 7
    zips = 8


class CleanBuild(Enum):
    at_src_change = 1
    everytime = 2
    never = 3


@total_ordering
class Version:
    v_map = {
        '13.0': ('cm-13.0',      '6.0.4'),
        '14.1': ('cm-14.1',      '7.1.2'),
        '15.1': ('lineage-15.1', '8.1'),
        '16.0': ('lineage-16.1', '9.0'),
        '17.1': ('lineage-17.1', '10.0'),
        '18.1': ('lineage-18.1', '11.0'),
        '19.1': ('lineage-19.1', '12'),
        '20.0': ('lineage-20.0', '13'),
    }

    def __init__(self, version: str):
        assert isinstance(version, str)
        # this is not consistent
        if version == '20':
            version = '20.0'
        assert version in self.v_map.keys(), f"Version {version} is missing."
        self._version = version

    def __eq__(self, other):
        return self._version == other._version

    def __lt__(self, other):
        return float(self._version) < float(other._version)

    def __repr__(self):
        return self.as_version()

    def as_version(self):
        return self._version

    def as_android_version(self):
        return self.v_map[self._version][1]

    def as_branch(self):
        return self.v_map[self._version][0]


class EmptyCallback():
    def __getattr__(self, attr):
        def no_function(*args, **kwargs):
            return
        return no_function


@dataclass
class DeviceConfig:
    codename: str
    version: Version

    proprietary: bool
    with_su: bool
    with_gms: bool

    android_jack_vm_args: str
    build_type: str

    patches: List[str]
    code: EmptyCallback  # actually: DeviceCode

    manifest: Path
    manifest_overrides: bool = False

    _mapping = {
        "proprietary": "INCLUDE_PROPRIETARY",
        "with_su": "WITH_SU",
        "with_gms": "WITH_GMS",
        "android_jack_vm_args": "ANDROID_JACK_VM_ARGS",
        "build_type": "BUILD_TYPE",
        "patches": "PATCHES",
        "manifest": "MANIFEST",
    }

    def _is_in_config(self):
        pass

    def get_raw(self, parm):
        return self._other_keys.get(parm.lower(), None)

    def __init__(self, device, dev_conf, general):
        self.codename = device.codename
        types = get_type_hints(self)

        def path_or_none(path: str):
            if path:
                return Path(path).absolute()
            return None

        ty_func = {bool: lambda x, y: x.getboolean(y),
                   str: lambda x, y: x.get(y),
                   Path: lambda x, y: path_or_none(x.get(y)),
                   List[str]: lambda x, y: x.get(y).split(',')}

        if dev_conf is None:
            dev_conf = {}
        self._other_keys = {**dict(general.items()), **dict(dev_conf.items())}

        # version is special:
        supported_vers = sorted(device.supported_versions, reverse=True)
        if "version" in dev_conf:
            wanted_version = dev_conf.get('VERSION')
        else:
            wanted_version = general.get('VERSION')
        if wanted_version == 'latest-official':
            for dev_vers in supported_vers:
                if dev_vers.status == Status.official:
                    self.version = dev_vers.version
                    quirks = dev_vers.quirks
                    break
            else:
                # fallback to latest if no official version found
                sv = next(iter(supported_vers))
                self.version = sv.version
                quirks = sv.quirks
        elif wanted_version == 'latest':
            sv = next(iter(supported_vers))
            self.version = sv.version
            quirks = sv.quirks
        else:
            self.version = Version(wanted_version)
            for sv in supported_vers:
                if sv.version == self.version:
                    quirks = sv.quirks
                    break
            else:
                log.error('Version %s not supported by device %s',
                          self.version,
                          device.codename)
        self._other_keys.pop("version", None)

        # assign values to attributes
        for key, other_key in self._mapping.items():
            func = ty_func[types[key]]
            if other_key in dev_conf:
                value = func(dev_conf, other_key)
                log.error(f"local {key} : {value}")
            elif quirks and quirks.affect(key):
                value = getattr(quirks, key)
                log.error(f"quirks {key} : {value}")
            else:
                value = func(general, other_key)
                log.error(f"general {key} : {value}")
            log.error(f"{key} : {value}")
            setattr(self, key, value)
            self._other_keys.pop(key, None)

        # quirks specific additions
        self.manifest_overrides = bool(quirks and quirks.manifest_overrides)
        if quirks and quirks.patches:
            self.patches.extend(quirks.patches)

        self.code = quirks.code if quirks and quirks.code else EmptyCallback()
        self.code.set_config(self)

        log.error(self)


class _Config:
    def __init__(self, filepath, script_dir):
        self.conf = ConfigParser(interpolation=ExtendedInterpolation())
        self.conf.read(filepath)
        self._script_dir = script_dir

    def get_codenames(self):
        devices = self.conf["General"]["DEVICE_LIST"]
        return devices.split(',')

    def get(self, device):
        subdevice = None
        if device.codename in self.conf:
            subdevice = self.conf[device.codename]
        dc = DeviceConfig(device, subdevice, self.conf["Devices"])
        return dc

    def get_dir(self, directory: Directory):
        if directory == Directory.anbu:
            return self._script_dir

        translation = {
            Directory.src: "SRC_DIR",
            Directory.data: "DATA_DIR",
            Directory.build: "BUILD_DIR",
            Directory.keys: "KEYS_DIR",
            Directory.ccache: "CCACHE_DIR",
            Directory.logs: "LOGS_DIR",
            Directory.zips: "ZIPS_DIR",
        }
        return Path(self.conf["General"][translation[directory]]).resolve().absolute()

    def get_key_subject(self):
        return self.conf["General"]["KEYS_SUBJECT"]

    def sign_builds(self):
        return self.conf["General"].getboolean("SIGN_BUILDS")

    def use_ccache(self):
        return self.conf["General"].getboolean("CCACHE")

    def clean_before_sync(self):
        return self.conf["General"].getboolean("CLEAN_BEFORE_SYNC")

    def clean_build_dir(self):
        return CleanBuild(self.conf["General"].get("CLEAN_BUILD_DIR").replace('-', '_'))

    def get_ccache_exec(self):
        return self.conf["General"].get("CCACHE_EXEC")

    def get_ccache_size(self):
        return self.conf["General"].get("CCACHE_SIZE")


class _DataConfig:
    def __init__(self, data_dir, script_dir):
        self._data_dir = data_dir
        self._script_dir = script_dir

    def get_dir(self, directory: Directory):
        if directory == Directory.data:
            return self._data_dir
        if directory == Directory.anbu:
            return self._script_dir
        assert False, "DataConfig can only provide the data dir"


def init_config(path, script_dir, just_data_dir=False):
    global _config
    assert _config is None, "init_config already called"
    if just_data_dir:
        _config = _DataConfig(path, script_dir)
    else:
        _config = _Config(path, script_dir)
    return _config


def get_config():
    global _config
    assert _config is not None, "call init_config before get_config"
    return _config
